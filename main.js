var async = require('async');
var fs = require('fs');
var fsextra = require('fs-extra')
var request = require('request');

var config = {
	outputDir: '/Users/jimmytsao/Desktop/TransferHtml/',


	pages: [
		{
			url: 'http://localhost:3000',
			outputFileName: 'home'
		},
		{
			url: 'http://localhost:3000/resources',
			outputFileName: 'resources'
		},
		{
			url: 'http://localhost:3000/experience',
			outputFileName: 'experience'
		},
		{
			url: 'http://localhost:3000/team',
			outputFileName: 'team-landing'
		},
		{
			url: 'http://localhost:3000/team/team',
			outputFileName: 'team-detail'
		},
		{
			url: 'http://localhost:3000/resources/advice',
			outputFileName: 'resources-advice'
		},
		{
			url: 'http://localhost:3000/resources/event',
			outputFileName: 'resources-event'
		},
		{
			url: 'http://localhost:3000/resources/documents',
			outputFileName: 'resources-documents'
		},
		{
			url: 'http://localhost:3000/resources/book-of-jargon',
			outputFileName: 'resources-book-of-jargon'
		},
		{
			url: 'http://localhost:3000/resources/faq-log',
			outputFileName: 'resources-faq-log'
		},
	]
};


// Remove Directory With Old Files
fsextra.remove(config.outputDir, function (err) {
  if (err) return console.error(err)

  	// Recreate Directory
	fsextra.mkdirs(config.outputDir, function (err) {
		if (err) return console.error(err)

		// Create parallel Tasks
		var tasks = config.pages.map(function(page){

			return function(callback){

				// Send a GET request to URL
				request(page.url, function(error, response, body){

					var pageName = config.outputDir + page.outputFileName + '.html';

					if (!error && response.statusCode == 200) {

						// Write the html text to a file
						fs.writeFile(pageName, body, function(err){
							if (err) throw err;

							console.log('Saved: ' + page.outputFileName + '.html');

							callback(null);
						});
					}
				})
			}
		});

		// Run tasks
		async.parallel(tasks, function(){
			console.log('Finished Saving All Pages');
		})
	});
})

