This utility program will save the HTML for a given url to a file

# Instructions
- Run `npm install`
- Customize the config object in the main.js file
- Run `node main.js`. If all of the pages in your config object were successfully saved then it should log `Finished Saving All Pages`
